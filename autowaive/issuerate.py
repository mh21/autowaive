"""DataWarehouse datastructure validation for auto-waiving tests."""

from typing import List

from rcdefinition.rc_data import DefinitionBase, _NO_DEFAULT


class DWIssueKind(DefinitionBase):
    """DW issue kind."""
    id: int = _NO_DEFAULT
    description: str = _NO_DEFAULT
    tag: str = _NO_DEFAULT


class DWIssueData(DefinitionBase):
    """DW issue data."""
    id: int = _NO_DEFAULT
    kind: DWIssueKind = _NO_DEFAULT
    description: str = _NO_DEFAULT
    ticket_url: str = _NO_DEFAULT
    resolved: bool = _NO_DEFAULT
    generic: bool = _NO_DEFAULT


class DWIssue(DefinitionBase):
    """DW issue high-level object."""
    id: int = _NO_DEFAULT
    issue: DWIssueData = _NO_DEFAULT
    pipeline: dict = _NO_DEFAULT
    testruns: list = _NO_DEFAULT
    bot_generated: bool = _NO_DEFAULT


class DWIssuesList(DefinitionBase):
    """A list of DW issues."""
    issues: List[DWIssue] = _NO_DEFAULT


class DWIssueRecords(DefinitionBase):
    """A list of DW issue records."""
    count: int = _NO_DEFAULT
    next: str = _NO_DEFAULT
    previous: str = _NO_DEFAULT
    results: DWIssuesList = _NO_DEFAULT
